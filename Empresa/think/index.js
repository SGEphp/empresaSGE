
function comprobar(f){
    var login = document.getElementById("login");
    var msgError1 = isEmpty(login);
    var pwd = document.getElementById("pwd");
    var msgError2 = isEmpty(pwd);
    
    // Ambos reciben nada de mensaje de error. Mesaje recivido del metodo de arriba
    if ( msgError1===""  && msgError2===""){
        //id_usuario,nombre,apellidos,login,pwd,tipo,pregunta,respuesta
        var objeto = new Usuarios(null,null,null, login.value,null,null,null,null );
        // Devolvera si existe un objeto con el login y si no el mismo pero con error de no encontrado
        ajaxUniversal("../utils/respuesta.php","empresa","usuarios","search",objeto,null,function(data){
            var r =filtro_if1Object(data);
            console.log(r);
//          // Si no lo encuentra te devolvera el mismo objeto  
            if (r.pwd != null){
                if (calcMD5(pwd.value) === r.pwd){
                    if (r.tipo=="administrador") 
                        document.getElementById("tipo").value="administrador";
                    else    
                        document.getElementById("tipo").value="normal";
                    
                    if(document.getElementById("remember").checked==true){
                        setCookie("user",document.getElementById("login").value,1);
                        setCookie("pwd",document.getElementById("pwd").value,1);
                        alert("La proxima vez que inicie sesión los datos serán recordados");
                    }
                    f.action="../pages/tipo.php";
                    f.submit();
                    
                }else msgError2+="La password no coincide";
            }else {
                msgError1+="El login esta incorrecto";
                msgError2+="  ";
            }
            lookTest(login, msgError1, false);
            lookTest(pwd, msgError2, false);
        });
    }else{
        console.log("Contienen errores");
    } 
    lookTest(login, msgError1, false);
    lookTest(pwd, msgError2, false);
}


function rellenarUsuarios(id_usuario,name,last,login,pwd,tipo,preg,resp){
    var usuario = {id_usuario:id_usuario, name:name, last:last,
        login:login, pwd:pwd, tipo:tipo, preg:preg, resp:resp};
}

///////////////////////////////////////////
/////////// C O O K I E //////////////////
//////////////////////////////////////////
function checkCookie(f) {
    var user=getCookie("user");
    var pwd=getCookie("pwd");
    //Si no esta vacia la cookie me pones los valores
    if (user != "") {
        f.login.value=user;
        f.pwd.value=pwd;
    } else {
       //alert("Todavia no hay cookie");
    }
    document.contenido.recuerdame.checked=false;
}
var d;
function setCookie(clave,valor,expira) {
    d = new Date();
    d.setTime(d.getTime() + (expira*60*60*1000)); // 60 * 1000 = 60 seg = 1 min * 60 = 1h * 24 = 24h
    var expires = "expires=" + d.toGMTString();
    document.cookie = clave+"="+valor+"; "+expires;
}
//fORMA de un cookie: "usuario=Natalia;password=** "
function getCookie(name) {
    var name = name+"=";
    var cooky = document.cookie.split(';');
    for(var x in cooky) {
        var c = cooky[x];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) {
            var valor = c.substring(name.length, c.length);
            return valor;
        }
    }
    return "";
}