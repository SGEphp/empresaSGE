// NECESITA UTILIDADES getTagElementPositionParentToDown

function nameLstnameTest(etiqueta){
    var msgError = "";
    msgError += isEmpty(etiqueta);
    
    lookTest(etiqueta, msgError, false);
    return msgError;
}

function pwdTest(etiqueta){
    var msgError = "";
    msgError += isEmpty(etiqueta);
    
    lookTest(etiqueta, msgError, false);
}

function mailTest(etiqueta){
    var msgError = "";
    msgError += isEmpty(etiqueta);
    msgError += ismailOk(etiqueta);
    
    lookTest(etiqueta, msgError, false);
}

//COMPROBACION DE .PNG
function extensionTest(etiqueta){
    var msgError = "";
    msgError += isExtensionFileOk(etiqueta,"png");
    alert(msgError);
    lookTest(etiqueta, msgError, false);
}

//COMPROBACION PAIS
function countryTest(etiqueta){
    var msgError = "";
    msgError += isSelectOption(etiqueta);
    
    lookTest(etiqueta, msgError, false);
}

//COMPROBACION CHECK RADIO
function typeTest(NameOnly){
    var msgError = "";
    msgError += isCheckedRadio(NameOnly);
    
    lookTest(document.getElementsByClassName("radioG")[1], msgError, true);
    
    return msgError;
}

// INPUT > Si mal o bien display SomeClass = que si tiene clase para ponerselo a .class erroneo o #por id
function lookTest(etiqueta, msgError, someClass){
    var posicionDIV;
    //Buscamos en etiquetas normales la posicion prmera porque es la unica
    //En radioButton (caso especial porque comienza con div) buscamos posicion 2 
    if(someClass==false) posicionDIV = getTagElementPositionParentToDown(etiqueta,"DIV",1);
    else posicionDIV = getTagElementPositionParentToDown(etiqueta,"DIV",2);
    
    if(msgError == ""){
        //document.fn.mail.parentNode.firstElementChild.className
        if(someClass==false)etiqueta.className = "validar";
        if(someClass==true)etiqueta.id = "validar";
        
    }else{
        if(someClass==false){
            etiqueta.className="erroneo";
            etiqueta.parentNode.childNodes[posicionDIV].innerHTML = msgError;
            
        }else{
            etiqueta.id="erroneo";
            etiqueta.parentNode.childNodes[posicionDIV].innerHTML = msgError;
        }
    }
}

function altaTest(f){
    var alta = true;
    if (nameLstnameTest(f.name) != "") alta=false;
    if (alta == true) f.submit();
}
/*
 //NIF ademas tiene que tener 9 digitos y usar trim
function cNif(etiqueta, advice){
    var cont = 0;
    var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
    var dni = etiqueta.value.trim();
    etiqueta.value=dni;
    if((/^\s*$/).test(dni)===true){
        etiqueta.className="erroneo";
        advice.innerHTML="No se puede dejar en blanco";
    }else if((/^(\d){8}[a-zA-Z]{1}$/).test(dni)===false){
        etiqueta.className="erroneo";
        advice.innerHTML="Formato inadecuado ******** numeros * letra";
    }else{
        if (letras[dni.substring(0,8)%23]==dni.charAt(8).toUpperCase()){
           etiqueta.className="validar";   
           
        }else{
            etiqueta.className="erroneo";
            advice.innerHTML="Letra o numero mal introducidos";
        }
    }
}
*/



/*


//Comparacion de que si es una clase f.name.className.indexOf('erroneo')===-1 cuando no existe
//Cuidado al cambiar el orden de NIF
function registrame(){
    var f = document.fn;
    var envio = true;
    if(f.name.className.indexOf('erroneo')!==-1 || (/^\s*$/).test(f.name.value)===true){
        envio=false;
        f.name.className="erroneo";
    }
    if(f.lst.className.indexOf('erroneo')!==-1 || (/^\s*$/).test(f.lst.value)===true){
        envio=false;
        f.lst.className="erroneo";
    }
    //este se valida al validar todo el formulario
    if(cNif(f.nif,document.getElementsByClassName('advice')[2])===false || f.nif.className.indexOf('erroneo')!==-1)envio=false;
    if(f.user.className.indexOf('erroneo')!==-1 || /^\s*$/.test(f.user.value)===true){envio=false;f.user.className="erroneo";}
    if(f.pwd.className.indexOf('erroneo')!==-1 || /^\s*$/.test(f.pwd.value)===true){envio=false;f.pwd.className="erroneo";}
    if(f.mail.className.indexOf('erroneo')!==-1 || /^\s*$/.test(f.mail.value)===true){envio=false;f.mail.className="erroneo";}
   
    // Checked : if its checked
    if (checkeo()!==true)envio=false;
    // If it's png  COMPROBAR SI ESTA VACIO 
    if(conArchivo(f.foto,document.getElementsByClassName('advice')[7])!==true){envio=false;}
    // If it's not 0
    if(selecter(f.pais,document.getElementsByClassName('advice')[8])===false){envio=false;}
    //alert("NO se puede dejar en blanco. Revisar los campos erroneos. El envio es "+envio);
    return envio;
}

 */

