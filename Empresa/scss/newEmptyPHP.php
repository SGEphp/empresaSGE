@font-face {
    font-family: "NovaCut";
    src: local("NovaCut"),
        url(../resources/fonts/Nova_Cut/NovaCut.tff) format('ttf');
}
ul { 
    margin: 0px;
    padding:0px;
    list-style: none;
    > li {
        > a {
            text-align: center;
            display: block;
            padding: 5px;
            text-decoration: none;
            color: white;
        }
    }
} 

.nav {
margin: auto;
width:90%;
margin-top: 3%;
> li {   
   
   padding: 3px;
   border-bottom: 2px solid white;
   height:1%;
   opacity: 0.8;
   float: left;
   width: 10%;
   //height: 1%;
   //background: black;
   
   
   line-height: 100%;
   
   > a {
       z-index:1;
       color: black;
       font-weight: bold;
       font-size: 13px;
       transition:0.8;
       &:hover{
           color:yellowgreen;
           font-size: 15px;
       }
   }
        &:hover ul li{
            display:block;
            position:relative;
            
            
        }

}
        >li ul li{
            display:none;
            > a:hover{
                color:black;
            }
        }
}