//EMPTY
function isEmpty(raiz){
    var msgError = "";
    if ((/^\s*$/).test(raiz.value)) msgError=" Debe poner algo.";
    return msgError;
}
////ONLY WORDS
function isWordsOnly(raiz){
    var msgError = "";
    if (!(/^[a-zA-Z]*$/).test(raiz.value)) msgError=" Solo deben ser caracter alfabetico.";
    return msgError;
}
// MUST SELECT ONE OPTION           > SELECT que no seleccione el primero
function isSelectOption(raiz){
    var msgError = "";
    if(raiz.value==0) 
        msgError = " Debe seleccionar una opcion.";
    return msgError;
}
// MUST CHECK ONE OPTION           > CHECK dame un name para localizar
function isCheckedRadio(NameOnly){
    var msgError = "";
    var r = getCheckValue_N(NameOnly);
//    No recibe el valor por tanto vacio
    if(r == "")
        msgError = "Por favor, seleccione una opcion";
    return msgError;
}
// EXTENSIONES
function isExtensionFileOk(raiz,extension){
    var msgError = "";
    if(raiz.value.substr(-4,4)!='.'.concat(extension)) 
        msgError = " Error. Extension requerida: "+extension;
    return msgError;
}

// PASSWORD
// // 1. Que tenga letra y numero. No admite caracteres
function ispwdOk(raiz){
    var msgError = "";
    if(!(/^(?=.*[a-z])(?=.*\d)\w*$/i).test(raiz.value)) 
        msgError = " No se corresponde con el formato: mayuscula/minuscula + numero.";
    return msgError;
}
      
// CORREO
function ismailOk(raiz){
    var msgError = "";
    if((/^(\w)+@(\w)+\.[a-zA-Z]{2,4}$/).test(raiz.value)===false)
        msgError = "El campo requiere c:caracter de c @ c . cx2 minimo.";
    return  msgError;
}

