// TAKE THIS : toma ese valor la funcion
function crearSelect(name, id, opciones){
    
}
/* CARGAR SELECT 
 * Variables: definida por ->
 * 
 * Estructura:
 *      VALUE y TEXT = SAME <option value="same">same</option>
 *          ! NOT SAME : value=bynumber (true)                  ->  valueNumerico
 *      SELECT:  by value o index numerico (parseado)           ->  selecionValor
 *
 *  Observaciones: da igual raiz por id o por name || DEVUELVE!! la raiz para que lo establezca y este ok
 */
//EXAMPLE document.getElement.. , myarraydevalores, false, null
function cargarSelect(raiz, arrayACargar, valueNumerico, selecionValor){
    var selector = raiz;
    for (var i = 0; i < arrayACargar.length; i++) {
        var op = document.createElement("option");
        if(valueNumerico==false) op.value=arrayACargar[i];
        else op.value=i;
        op.innerHTML=arrayACargar[i];
        selector.appendChild(op);
    }
    selector = setValueSelect(selector, selecionValor);

}

function setValueSelect(raiz, selecionValor){
    var selector = raiz;
    if (selecionValor != null && isNaN(selecionValor))
        selector.value = selecionValor;
    if (selecionValor != null  && isNaN(selecionValor)==false)
        selector.selectedIndex = parseInt(selecionValor);
    return selector;
}

/* TOMAR VALOR SELECT
 *          <option value="numerico/valor">TAKE THIS</option>
 * Estructura:
 *      SELECT:  numerico          ->  selecionValor
 *
 *  Observaciones: _N busqeda por numero
 *  
 */
function getTextSelect_INDEX(raiz, selecionValor_N){
    var valor;
    // por INDEX
    if (isNaN(selecionValor_N)==false)
        valor = raiz.options[parseInt(selecionValor_N)].text;
    else
       valor = "Hubo un error con el formato. Index no encontrado";
   return valor;
}


function getValueSelectSelecionado(raiz){
    return raiz.value;
}

