

function nodoTxt(t) {
    return document.createTextNode(t);
}
// EJEMPLO: document.getElementById (...) , [Objeto1], [columnaNº1,Nº3], inputTRUE
function tableUtilsCSS3_model1(raiz, arrayObjects, omitir, input) {
    
    var omitir = omitir;// no omitir nada = -1 Omitir array int
    var header = true;
    // Para deficir celda 1 cabecera SIEMPRE a true
    var directriz = true;

    var objetos = arrayObjects;
    var tabla = document.createElement("table");
    tabla.setAttribute("table-layout","fixed");
   //tabla.className = "responstable";
    tabla.border = 1;
    // Directriz es obligatoria definirla por si hay si hay header
    //tabla.width="-60%";
    
    // Ha de ser asi para que no se salte ninguna fila y se le pueda hacer --
    for (var i = 0; i < objetos.length; i++) {
        var fila = document.createElement("tr");
        var o = objetos[i];
        // Con cada caja va aumentando y a cero cuando cambia de fila
        var omit = 0;
        // Con cada coincidencia va aumentando y a cero cuando cambia de fila
        var indexOmit = 0;
        for (var p in o) {
            var casilla;
            // Entre corchetes que igual no te lo coge
            if (omit != omitir[indexOmit]) {
                // Lo que se ancla al final sea input o texto CASILLA.APENDCHILD
                var text;
                // Si hay cabecera el elemento es TH
                if (header != true)  casilla = document.createElement("td");
                else                 casilla = document.createElement("th");
                // Elegimos si es texto o input para modificar
                if (header != true){
                    if (input==true){
                        text = inputUtils(p,p,o[p]);
                    }else{
                        text = nodoTxt(o[p]);
                    }
                    
                // Si header = true es que queremos cabeceras descriptorias en la tabla
                }else {
                    // Casilla directriz para que salga la cabecera que el la propiedad del objeto
                    if (directriz == true) {
                        casilla.setAttribute("data-th", "Driver details");
                        text = document.createElement("span");
                        text.appendChild(nodoTxt(p));
                        directriz = false;
                    // Casilla donde se ubican los valores de cada casilla    
                    } else{ 
                        text = nodoTxt(p);
                    }
                    
                    
                }
//                casilla.setAttribute("width","10px");
                casilla.appendChild(text); // solo el nodo texto
                fila.appendChild(casilla);
            } else indexOmit++;
            omit++;
        }
        tabla.appendChild(fila);
        if (header == true) { i--;
            header = false;
        }
    }
    raiz.appendChild(tabla);

}

// Se necesita setAttributes
function inputUtils(name, id, value ){
    var input = document.createElement("input");
    setAttributes(input, {"type": "text",
//        "width":"10px",
        "id": id,
        "name": name,
        "value": value});
    return input;
}
