//setCheck_Id el _ el por value ==> seleccion por valor busca ese valor

/* CARGAR SELECT 
 * Variables: definida por ->
 * 
 * Estructura:
 *          Permite seleccionar valor de entrada valor check
 *          ! Si hay label o no   LABEL = NAME                  ->  label (boolean)
 *          id = value      
 *          null , INDEX, id -> checked
 *Ejemplo   <input type='radio' id='f' name='sexo' value='f'> Mujer
 *  Observaciones: 
 *  Tienes que hacer append al root (DIV) porque se termina la etiqueta del input
 *   DEVUELVE!! la raiz para que lo establezca y este ok
 */
function cargarCheckBOX(raiz, label, name, arrayValues, arrayText, checked ){
    var selector = raiz;
    var i = 0;
    if (label==true) selector = addLabel(selector, name);
    
    for (var i = 0; i < arrayValues.length; i++){
        var elemento = document.createElement("input");
        setAttributes(elemento, {"type": "radio", "id": arrayValues[i], "name": name, "value": arrayValues[i]});
        selector.appendChild(elemento);
        selector.appendChild(document.createTextNode(arrayText[i]));
    }
    
    // Distinto de nulo y es un INDEX y marca (true=con palomita false=sinella)
    if (checked != null  && !isNaN(checked)) setCheck_INDEX(
           document.getElementsByName(name), checked, true);
    else if (checked != null  && isNaN(checked)) setCheck_Id(checked, true);
        
}

function addLabel(raiz, name){
    var label = document.createElement('label');
    label.htmlFor = name;
    label.appendChild(document.createTextNode(name));
    raiz.appendChild(label);
    return raiz;
}
//<input type="radio" id="M" name="sexo" value="M"/> M
// marca -> true para marcar o false
function setCheck_Id(id, marca){
    // Si es una letra no se puede hacer por index
    if(isNaN(id)){
        if (marca == true) document.getElementById(id).checked=true;
        else document.getElementById(id).checked=false;
    } else {
        alert("El formato de check no es una letra");
    }
}
// EXAMPLE FINAL:  setCheck_INDEX(document.getElementsByName(name), checked, true);
function setCheck_INDEX(root, index, marca){
    // Si es una letra no se puede hacer por index
    if(isNaN(index)){
        alert("El formato de check no es un numero. Ha de ser un numero");
    } else {
        if (marca == true) root[parseInt(index)].checked=true;
        else root[parseInt(index)].checked=false;
    }
}
// Tomamos el valor que consultamos y si no hay valor devolvemos null y un mensaje por consola
// por si acaso
function getCheckValue_N(NameOnly){
    var valor = "";
    try{
        console.log(document.getElementsByName(NameOnly).value);
        valor = document.querySelector('input[name = '+NameOnly+']:checked').value;
    }catch (err){
        console.log(err);
    }
    return valor;
}



// Otra manera mas lenta
//function checkeo(){
//    if(document.getElementById("F").checked==true || document.getElementById("M").checked==true){

//}

//function setCheck_Value(){
//    if(isNaN(index)){
//        if (marca == true) root[parseInt(index)].checked=true;
//        else root[parseInt(index)].checked=false;
//    } else {
//        alert("El formato de check no es una letra");
//    } 
