<?php

include "../bd/Sentences.inc";
$envio = "";
$affectedrow="";
$unaFila = false;
$database = filter_input(INPUT_POST, 'database');
$table = filter_input(INPUT_POST, 'table');

$objeto = json_decode(filter_input(INPUT_POST, 'objeto'));
$tipo = filter_input(INPUT_POST, 'tipo');
// Para tipo update decimos cual es la clave principal que buscamos y getOneField
$pkNumb = filter_input(INPUT_POST, 'pkNumb');

if ($tipo != "update" && $tipo != "delete") {
    $con = mysqli_connect("localhost", "root", "", $database);
    $con->query("SET NAMES utf8");
    // ESTADO CONEXION
    if (!$con)
        die("La conexion no tuvo exito: " . mysqli_connect_error());
}
//  (MySQLi Procedural) LAS DOS PRIMERAS y last
// (MySQLi Object-oriented) penul
//$sql = "";
//$affectedrow = "";
switch ($tipo) {
    // Si pones casilla 0 en el bojeto pon el id a identificar
    case "getOneField":
        $sql = Sentences::create_get_oneField($table, $objeto, $pkNumb);
        $filas = mysqli_query($con, $sql);
        $valor;
        // Las 
        try {
            if (!isset($filas->num_rows) ) {
                throw new Exception("No result was found for id:$filas");
            }
            $array=[];
            if ($filas->num_rows > 0)
                $unaFila = true;
            else {
                $objeto->{"error"} = "No se encontraron resultado. No hay datos que encontrar. Fuente vacia";
                $envio .= json_encode($objeto);
            }
            if ($unaFila == true) {
                while ($row = mysqli_fetch_array($filas, MYSQLI_BOTH)) {
                    if ($row != null) {
                        $i = 0;
                        if ($i == 0)
                            $array[] = $row[0];
                        else
                            array_push($array, $row[0]);
                        $i++;
                    }
                }
                $envio = json_encode($array);
            }
        } catch (Exception $e) {
            $objeto->{"error"} = "No se encontraron resultados que coincidan. No se definio bien la pk y por tanto mal la consulta";
            $envio .= json_encode($objeto);
        }
        break;
        mysqli_close($con);
    case "search":
        // Devuelve null si no encontro nada
        $sql = Sentences::create_search($table, $objeto);
        $filas = mysqli_query($con, $sql);
        if ($filas->num_rows > 0)
            $unaFila = true;
        else {
            $objeto->{"error"} = "No se encontraron resultados";
            $envio .= json_encode($objeto);
        }
        if ($unaFila == true) {
            $array = array();
            while ($row = mysqli_fetch_array($filas, MYSQLI_BOTH)) {
                $objeto1 = clone $objeto;
                foreach ($objeto1 as $key => $value) {
                    if ($key != "error")$objeto1->{$key} = $row[$key];
                }
                array_push($array, $objeto1);
            }
            $envio = json_encode($array);
        }
        mysqli_close($con);
        break;
    case "add_auto":
        $sql = Sentences::create_insert($table, $objeto, true);
        break;
    case "add":
        // Control de una clase llamada ERROR que la quita de la formacion de la sentencia sql y control de PWD a md5
        //Php te devuelve una informacion diciendo que la primary key esta repetida. Por eso esta vacio
        $sql = Sentences::create_insert($table, $objeto, false);

        break;
    case "update": // Unica forma de devolver filas afectadas: 0 = no hay update 1 = si hay cambios
        $query = Sentences::create_update($table, $objeto, $pkNumb);

        break;
    case "delete":
        $query = Sentences::create_delete($table, $objeto);

        break;
}
if ($tipo == "add" || $tipo == "add_auto") {
    $fila = mysqli_query($con, $sql);
    if (!$fila)
        $objeto->{"error"} = "No se pudo dar de alta. Pongase en contacto con el administrador";
    $envio = json_encode($objeto);
    mysqli_close($con);
}
if ($tipo == "update" || $tipo == "delete") {
    // pkNumb primary key donde hacer where alisan y los valores no nulos se cambiaran
    // $query = update usuarios set nombre='popo',apellidos='sorrosa',pwd='3c374c2ef83922b2c0ff8ae8de0b315e' where login='alisan'
    // $objeto = new Usuarios(null,"popo","sorrosa", "alisan","ernaga",null,null,null );
    $mysqli = new mysqli("localhost", "root", "", $database);
    /* check connection */
    if (mysqli_connect_errno()) {
        $objeto->{"error"} = "Conexion fallida";
    }
    if ($stmt = $mysqli->prepare($query)) {

        /* Se sustituye por la interrogacion */
        //    $code = 'A%';
        //    $stmt->bind_param("s", $code);
        $stmt->execute();
        //Devolvera 0 si no encuentra nada que cambiar
        $affectedrow .= $stmt->affected_rows;
        $stmt->close();
        $mysqli->close();
    }

    if ($affectedrow == 0){
        if($tipo === "delete")
        $objeto->{"error"} = "No se produjo ningun cambio en el eliminado. Revise los datos ";
        else
        $objeto->{"error"} = "No se produjo ningun cambio en el alta. Revise los datos ";    
    }
    $envio = json_encode($objeto);
}



echo $envio;
?>
