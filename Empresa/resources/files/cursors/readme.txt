﻿=== Flowing Transparent Rainbow Cursor Set ===

By: winger (http://www.rw-designer.com/user/16070)

Download: http://www.rw-designer.com/cursor-set/flowing-transparent-rainbow

Author's decription:

These are the improved version of Transparent Rainbow Cursors.  The spectrum animation is smoother, cleaner and moves outward instead of straight across.  

==========

License: Creative Commons - Attribution

You are free:

* To Share - To copy, distribute and transmit the work.
* To Remix - To adapt the work.

Under the following conditions:

* Attribution - You must attribute the work in the manner specified
  by the author or licensor (but not in any way that suggests that
  they endorse you or your use of the work). For example, if you are
  making the work available on the Internet, you must link to the
  original source.