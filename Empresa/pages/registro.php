<html>
    <head>
        <meta charset="UTF-8">
        <title>Registrate</title>
        <link rel="stylesheet" type="text/css" href="../css/styleRegistro.css"/>
        <!--JS PROPIOS-->
        <script type="text/javascript" src="../think/validame.js"></script>
        <!--UTILIDADES-->
        <script type="text/javascript" src="../utils/selectUtils.js"></script>
        <script type="text/javascript" src="../utils/utilidades.js"></script>
        <script type="text/javascript" src="../utils/checkboxUtils.js"></script>
        <!--# Validacion -->
        <script type="text/javascript" src="../utils/validacionUtils.js"></script>
        <script type="text/javascript" src="../think/validame.js"></script>
         <!--Datos persistentes : Paises-->
        <script type="text/javascript" src="../bd/datos.js"></script>
        <script type="text/javascript">
            // Cargar paises y las opciones de las preguntas
            function carga(){
                cargarSelect(document.fn.pais,getPaisesArray(),false,"Spain");
                cargarSelect(document.getElementById("preg"), getPreguntas(),true, null);
                
                var sValues = new Array("F","M");
                var sText = new Array("Mujer","Hombre");
                cargarCheckBOX(document.getElementsByClassName("radioG")[0], true, "Sexo", sValues, sText, null);
                
                var tTextValues = new Array("Administrador","Normal");
                //cargarCheckBOX(document.getElementById("div9"), true, "Tipo", tTextValues, tTextValues, null);
            }
            

        </script>
    </head>
    <body onload="carga()" 
          style="background-image: url(../resources/files/empresa.jpg);background-repeat: no-repeat;background-size: 100% 100%;">
        <header><div id="titulo">Registro de Usuarios</div></header>
        <form method="post" class="formulario"  id="fn" name="fn" action="procesar_alta.php">
            <div id="div0">
                <!--//No cambiar el orden ya que determina el errorLookTest. YA SE PUEDEE!! childNodes[posicionDIV]. ::> function lookTest(etiqueta, msgError) -->
                <input type="text" id="name" name="name" onkeyup="nameLstnameTest(this)"/>
                <label for="name">Name</label>
                <div class="advice"></div>
            </div>
            <div id="div1">
                <input type="text" id="lst" name="lst" onkeyup="nameLstnameTest(this)"/>
                <label for="lst">Last</label>
                <div class="advice"></div>
            </div>
            <div id="div2">
                <input type="text"  id="nif" name="nif" />
                <label for="nif">N.I.F</label>
                <div class="advice"></div>
            </div>
            <div id="div3">
                <input type="text"  id="user" name="user" onblur=""/>
                <label for="user">User</label>
                <div class="advice"></div>
            </div>
            <div id="div4">
                <input type="password"  id="pwd" name="pwd" onblur="pwdTest(this)"/>
                <label for="pwd">PassWord</label>
                <div class="advice"></div>
            </div>      
            <div id="div5">
                <input type="text" id="mail" name="mail" onblur="mailTest(this)"/>
                <label for="mail">E-Mail</label>
                <div class="advice"></div>
            </div >
            <div id="div6">
                <div class="radioG">
                    
                </div>
            </div>
            <div id="div7">
                <input  type="file"  name="foto" id="foto" onchange="extensionTest(this)">
                <label for="foto" style="margin-left: 2%;">Archivo </label>
                <div class="advice"></div>
            </div>
            <div id="div8">
                <select name="pais"  id="pais" onchange="countryTest(this)">
                    <option value="0">-Elegir pais-</option>
                </select>
                <label for="pais">Pais</label>
                <div class="advice"></div>
            </div>
            <div id="div9">
                <div class="radioG" id="a">
                    <ul>
                      <li>
                          <input type="radio" id="admin" name="tipo" value="admin" >
                          <label for="admin">Administrador</label>
                          <div class="circlePers"></div>
                      </li>
                      <li>
                          <input type="radio" id="normal" name="tipo" value="normal">
                          <label for="normal">Normal</label>
                          <div class="circlePers"></div>
                      </li>
                    </ul>
                </div>
                <div class="advice"></div>
            </div>
            <div id="div10">
                <select name="preg"  id="preg" onchange="countryTest(this)">
                    
                </select>
                <label for="preg">Pregunta</label>
                <div class="advice"></div>
            </div>
            <div id="div11">
                <input type="text"  id="resp" name="resp"/>
                <label for="respuesta">Respuesta: </label>
                <div class="advice"></div>
            </div>
            
            <div id="btn-foot">
                <input type="button"  id="btnB" name="btnB"  onclick="location.href='../empresa'" value=" < B A C K " />
                <input type="button" id="btnU" name="btnU" value="O K E Y ! Sign Me UP" onclick="altaTest(document.fn)"/>
                
            </div>
        </form>
       <!--//<button onclick="altaTest()">O K E Y !<button/>-->
    </body>
</html>