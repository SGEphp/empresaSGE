<html>
    <head>
        <script type="text/javascript" src="../../utils/clases.js"></script>

        <link rel="stylesheet" type="text/css" href="../../css/tableStyle.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/styleRegistro.css"/>
        <!--UTILIDADES: select-->
        <script type="text/javascript" src="../../utils/selectUtils.js"></script>
        <!--UTILIDADES: ajax -->
        <script type="text/javascript" src="../../utils/utilidades.js"></script>
        <!--UTILIDADES : tabla dinamica-->
        <script type="text/javascript" src="../../utils/buildDinamically.js"></script>
        <!--UTILIDADES : dato array pregunta -->
        <script type="text/javascript" src="../../bd/datos.js"></script>
        <script type="text/javascript">
            var id;
            var respuesta;
            function cargarPreg() {
                var login = document.getElementById("rec").value;
                if (login!=""){
                    var objeto = new Usuarios(null, null, null, login, null, null, null, null);
                    ajaxUniversal("../../utils/respuesta.php", "empresa", "usuarios", "search", objeto, null, function (data) {
                        var r = filtro_if1Object(data);
                        console.log(r);
                        id = r.id_usuario;
                        console.log(id);
                        console.log(r.respuesta);
                        if (r.error!=null || r.error!=""){
                            var preg = r.pregunta;
                            var pregBD = getPreguntas();
                            var index = parseInt("0");
                            
                            for(var x in pregBD){
                                if(index==preg){
                                    document.getElementById("pr").innerHTML=pregBD[x];
                                    respuesta=r.respuesta;
                                    document.getElementById("btnU").setAttribute("onclick","modificar()");
                                    document.getElementById("rec").value="";
                                    document.getElementById("btnU").value="confirmar";
                                    break;
                                }
                                index=index+1;
                            }
                        }
                    });
                }else{
                    alert("Login vacio");
                }
            }

            function modificar() {
                var rUser = document.getElementById('rec').value;
                console.log(id);
                if(respuesta!==rUser){
                    alert("Las respuestas no coinciden. Pongase en contacto con el administrador");
                    window.location = "http://localhost:8079/Empresa/empresa/index.php";
                }else{
                    document.getElementById("pr").innerHTML="Introduzca la nueva contraseña";
                    document.getElementById("rec").value="";
                    document.getElementById("btnU").setAttribute("onclick","update()");
                    document.getElementById("btnU").value="renovar";
                }
            }

            function update(){
                var objeto = new Usuarios(id, null, null, null, document.getElementById('rec').value, null, null, null);
                    ajaxUniversal("../../utils/respuesta.php", "empresa", "usuarios", "update", objeto, null, function (data) {
                        alert("La contraseña fue modificada");
                        window.location = "http://localhost:8079/Empresa/empresa/index.php";
                    });
            }
        </script>
        <style>
            table{
                vertical-align: middle;
            }
        </style>
    </head>
    
    <body  >
        <div class="formulario">
            <div id="div1">
                <p id="pr">Introduzca el login</p>
                <input type="text" name="rec" id="rec"/>    
                <div class="advice"></div>
            </div>
            <div id="btn-foot">
                <input type="button" id="btnU" name="btnU" value="Enviar dato" onclick="cargarPreg()"/>
            </div>
        </div> 
        <div id="div2">

        </div>
    </body>
</html>
