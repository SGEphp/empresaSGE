<html>
    <head>
        <script type="text/javascript" src="../../utils/clases.js"></script>
           
        <link rel="stylesheet" type="text/css" href="../../css/tableStyle.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/styleRegistro.css"/>
        <!--UTILIDADES: select-->
        <script type="text/javascript" src="../../utils/selectUtils.js"></script>
        <!--UTILIDADES: ajax -->
        <script type="text/javascript" src="../../utils/utilidades.js"></script>
        <!--UTILIDADES : tabla dinamica-->
        <script type="text/javascript" src="../../utils/buildDinamically.js"></script>
        <script type="text/javascript">
            var loginId;
            function cargarLogin() {
                var objeto = new Usuarios(null, null, null, null, null, null, null, null);
                ajaxUniversal("../../utils/respuesta.php", "empresa", "usuarios", "search", objeto, null, function (data) {
                    var r = data;
                    console.log(r);
                    // Login es la columna 3
                    // Cogemos el array de login para cargar el array
                    var login = [];
                    var index = 0;
                    for(var x in r){
                        var contador = 0;
                        var objeto = r[x];
                        
                        for (var atr in objeto){
                            if (contador==3){
                                login[index]=objeto[atr];
                                index++;
                            }
                            contador++;
                        }
                    }
                    cargarSelect(document.getElementById("log"), login,false,null);
                 });
            }
            
            function modificar(valor){
                var objeto = new Usuarios(null, null, null, valor, null, null, null, null);
                ajaxUniversal("../../utils/respuesta.php", "empresa", "usuarios", "search", objeto, null, function (data) {
                    var r = data;
                    console.log(r);
                    // Valores a omitir siempre en array
                    loginId= filtro_if1Object(r).id_usuario;
                    console.log("Su id usuario es: "+loginId);
                    tableUtilsCSS3_model1(document.getElementById("div2"),r, [0,3,8], true);
                    
                    document.getElementById("pwd").value=" ";
                    
                });
            }
            
            function modifica(){
                console.log("PASO 2 > Su id usuario es: "+loginId);
                var nombre = document.getElementById("nombre").value;
                var apellidos = document.getElementById("apellidos").value;
                var tipo = document.getElementById("tipo").value;
                var pregunta = document.getElementById("pregunta").value;
                var respuesta = document.getElementById("respuesta").value;
                var objeto = new Usuarios(loginId, nombre, apellidos, null, null, pregunta, respuesta, null);
                ajaxUniversal("../../utils/respuesta.php", "empresa", "usuarios", "update", objeto, 0, function (data) {
                    alert("Usuario modificado correctamente");
                });
            }
        </script>
        <style>
            table{
                vertical-align: middle;
            }
        </style>
    </head>
    <!--cargarlogin-->
    <body onload="cargarLogin()" >
        <div class="formulario">
            <div id="div1">
                <select name="log"  id="log" onchange="modificar(document.getElementById('log').value)" >
                    
                </select>
                <label for="log">Login</label>
                <div class="advice"></div>
            </div>
            <div id="btn-foot">
                <input type="button" id="btnU" name="btnU" value="Modificar" onclick="modifica()"/>
            </div>
        </div> 
        <div id="div2">
            
        </div>
    </body>
</html>
