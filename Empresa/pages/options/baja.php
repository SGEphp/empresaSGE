<html>
    <head>
        <script type="text/javascript" src="../../utils/clases.js"></script>
        
        <link rel="stylesheet" type="text/css" href="../../css/styleRegistro.css"/>
        <!--UTILIDADES: select-->
        <script type="text/javascript" src="../../utils/selectUtils.js"></script>
        <!--UTILIDADES: ajax -->
        <script type="text/javascript" src="../../utils/utilidades.js"></script>
        <!--UTILIDADES : tabla dinamica-->
        <script type="text/javascript" src="../../utils/buildDinamically.js"></script>
        <script type="text/javascript">
            function cargarLogin() {
                var objeto = new Usuarios(null, null, null, null, null, null, null, null);
                ajaxUniversal("../../utils/respuesta.php", "empresa", "usuarios", "search", objeto, null, function (data) {
                    var r = data;
                    console.log(r);
                    // Login es la columna 3
                    var login = [];
                    var index = 0;
                    for(var x in r){
                        var contador = 0;
                        var objeto = r[x];
                        
                        for (var atr in objeto){
                            if (contador==3){
                                login[index]=objeto[atr];
                                index++;
                            }
                            contador++;
                        }
                        
                    }
                    
                    cargarSelect(document.getElementById("log"), login,false,null);
                });
            }
            function baja(valor){
                var result = confirm("¿Estas seguro de querer borrarlo?");
                if (result){
                    borrarUsuario(valor);
                }
                cargarLogin();
            }
            
            function borrarUsuario(login){
                var objeto = new Usuarios(null, null, null, login, null, null, null, null);
                ajaxUniversal("../../utils/respuesta.php", "empresa", "usuarios", "delete", objeto, null, function (data) {
                    var r = data;
                    if (data.error == null){
                        alert("Usuario borrado satisfactoriamente");
                    }else{
                        alert(r.error);
                    }

                });
            }

        </script>
    </head>
    <body onload="cargarLogin()" >
        <div class="formulario">
            <div id="div1">
                <select name="log"  id="log" >
                    
                </select>
                <label for="log">Login</label>
                <div class="advice"></div>
            </div>
            <div id="btn-foot">
                <input type="button" id="btnU" name="btnU" value="Dar de baja" onclick="baja(document.getElementById('log').value)"/>
                
            </div>
        </div> 
    </body>
</html>