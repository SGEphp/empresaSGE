<html>
    <head>
        <link rel="stylesheet" type="text/css" href="../../css/tableStyle.css"/>
        <script type="text/javascript" src="../../utils/utilidades.js"></script>
        <script type="text/javascript" src="../../utils/clases.js"></script>
        <!--UTILIDADES : tabla dinamica-->
        <script type="text/javascript" src="../../utils/buildDinamically.js"></script>
        <script type="text/javascript">
            function cargarTabla() {
                var objeto = new Usuarios(null, null, null, null, null, null, null, null);
                ajaxUniversal("../../utils/respuesta.php", "empresa", "usuarios", "search", objeto, null, function (data) {
                    var r = data;
                    var myArray = r[0];
                    // Valores a omitir siempre en array
                    var omisionLista = [0,2,4,6,7,8];
                    tableUtilsCSS3_model1(document.body,r, omisionLista, false);

                });
            }
           
            

        </script>
        
    </head>
    <body onload="cargarTabla()" >
        <h1>Listado <span>Usuarios</span></h1>
    </body>
</html>