<?php
class Sentences {
    public static function create_insert($table,$objeto,$skipFirst)
   {
    $tipo_insertar = "insert into ".$table." (";
    $valores = "";
        foreach($objeto as $key => $var){
            if($skipFirst!=TRUE && $key!="error"){
                $tipo_insertar.=$key.",";
                if($key=="pwd"){$valores.= "'". md5 ($var)."',";}
                else {
                    $valores.="'".$var."',";
                }
            }
            if($skipFirst==TRUE){
                $skipFirst=false;
                
            }
        }
    $tipo_insertar = substr($tipo_insertar,0,-1).") values (".substr($valores,0,-1).")";
    return $tipo_insertar;
   }


   
   public static function create_search($table,$objeto){
       $tipo_buscar = "select * from ".$table." where ";
        foreach ($objeto as $key =>$var ) {
            if ($var != null ) {
                $tipo_buscar .= $key."='".$var."' and ";
            }
        }
        $tipo_buscar = substr($tipo_buscar,0,-5);
        return $tipo_buscar;
   }
   
   public static function create_get_oneField($table,$objeto,$number){
       $tipo_oneField ="";
       
       $i=0;
        foreach ($objeto as $key =>$var ) {
            
            if ($i == $number ) {
                $tipo_oneField = $key;break;
            }
            $i++;
        }
        return "select ".$tipo_oneField." from ".$table;
   }
   
   public static function create_update($table,$objeto,$pkNumb ){
        $cont=0;
        $donde="";
        $tipo_update = "update ".$table." set ";
        foreach ($objeto as $key => $var) {
            if ($key!="error" && $var != null && $cont!=$pkNumb ) {
                if($key=="pwd"){$tipo_update.= $key."='". md5 ($var)."',";}
                else $tipo_update .= $key."='".$var."',";
            }
            if($cont==$pkNumb) $donde = " where ".$key."='".$var."'";
            $cont++;
        }
        $tipo_update = substr($tipo_update,0,-1).$donde;
        return $tipo_update;
   }
   
   public static function create_delete($table,$objeto){
        $tipo_delete = "delete from ".$table." where ";
        foreach ($objeto as $key => $var) {
            if ($var != null ) {
                $tipo_delete .= $key."='".$var."' and ";
            }
        }
        $tipo_delete = substr($tipo_delete,0,-5);
        return $tipo_delete;
   }
}
?>
