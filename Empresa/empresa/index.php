<html>
    <head>
        <title>Bienvenido a Sistema de Gestion Empresarial</title>
        <link type="text/css" rel="stylesheet" href="../css/styleportal.css"/>
        <!-- # Javascript asociados-->
        <script type="text/javascript" src="../think/index.js"></script>
        <!-- # Utilidades: utilidades (AjaxUniversal,montante,enconde,decod) || md5 pwd || clases || cookies-->
        <script type="text/javascript" src="../utils/utilidades.js"></script>
        <script type="text/javascript" src="../utils/clases.js"></script>
        <script type="text/javascript" src="../utils/md5.js"></script>
        <!--# Validacion de campos vacios-->
        <script type="text/javascript" src="../utils/validacionUtils.js"></script>
        <script type="text/javascript" src="../think/validame.js"></script>
        <!--# Cookies-->
    </head>
    <body onload="checkCookie(document.form)">
        <form class="formulario" method="post" id="form" name="form">
            <input name="tipo" id="tipo" style="visibility:hidden"/>
            <div>
                <input type="text"  id="login" name="login" />
                <label for="login">Login</label>
                <div class="advice"></div>
            </div>
            <div>
                <input type="password" id="pwd" name="pwd"  />
                <label for="pwd">PassWord</label>
                <div class="advice"></div>
            </div>
            <div>
                <a href="../pages/registro.php">¿No te has registrado?</a>
                <a href="../pages/options/recover.php">Se me ha olvidado la contraseña</a>
            </div>
            <div>
                <input type="checkbox" name="remember" id="remember"/>Recuerdame
            </div>
            <div id="btn-foot">
                <input type="button" id="btnU" name="btnU" value="E N T R A D A" onclick="comprobar(document.form)"/>
            </div>
        </form>
    </body>
</html>


